#ifndef _ULVT_TRANSPOSE_H_
#define _ULVT_TRANSPOSE_H_

#ifdef __cplusplus
extern "C" {
#endif

void ulvt_transpose_v1(void *out, void *in);

#ifdef __cplusplus
}
#endif

#endif // _ULVT_TRANSPOSE_H_
