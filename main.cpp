#include <benchmark/benchmark.h>

#include <cstring>
#include <math.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>

// Benchmarking 2^12 x 2^11th matrices (Hermez)

static const size_t matrix_columns = 672;
static const size_t matrix_rows = 2048 * 4096;
//static const size_t matrix_rows = 512 * 1024;

// region - utils

#include <sys/mman.h>

void* alloc_matrix(size_t bytes) {
  assert(bytes % 4096 == 0);

  int flag = MAP_ANONYMOUS | MAP_PRIVATE;
#if POPULATE
  flag |= MAP_POPULATE;
#endif
#if HUGETLB
  flag |= MAP_HUGETLB;
#endif
  void* result = mmap(NULL, bytes, PROT_READ | PROT_WRITE, flag, -1, 0);
  if (result == MAP_FAILED) {
    perror("something happened");
  }

#if LOCK
  int err = mlock(result, bytes);
  if (err != 0) {
    perror("something happened");
  }
  assert(err == 0);
#endif

#if RANDOM
  int rand = open("/dev/urandom", O_RDONLY);
  size_t total_read = 0;
  do {
    total_read += read(rand, (uint8_t*)result + total_read, bytes - total_read);
  } while (total_read < bytes);
  
  close(rand);
#endif

  return result;
}

void free_matrix(void* matrix, size_t bytes) {
  int err = munlock(matrix, bytes);
  assert(err == 0);
  err = munmap(matrix, bytes);
  assert(err == 0);
}

const size_t matrix_size = sizeof(uint64_t) * matrix_columns * matrix_rows;

static inline uint64_t* global_input_matrix() {
  static uint64_t* _matrix = nullptr;
  if (_matrix == nullptr) {
    _matrix = (uint64_t*)alloc_matrix(matrix_size);
  }
  return _matrix;
}

static inline uint64_t* global_output_matrix() {
  static uint64_t* _matrix = nullptr;
  if (_matrix == nullptr) {
    _matrix = (uint64_t*)alloc_matrix(matrix_size);
  }
  return _matrix;
}

// endregion

// region - memcpy


static void memcpy_bench(benchmark::State& state) {
  uint64_t* input_matrix = global_input_matrix();
  uint64_t* output_matrix = global_output_matrix();
    for (auto _: state) {
      memcpy(output_matrix, input_matrix, matrix_size);
    benchmark::ClobberMemory();
  }
}
BENCHMARK(memcpy_bench)->Name("memcpy")->UseRealTime()->Unit(benchmark::kSecond);

// endregion

// region - ulvt_transpose

#include "ulvt-transpose.h"

static void ulvt_transpose_v1_bench(benchmark::State& state) {
  uint64_t* input_matrix = global_input_matrix();
  uint64_t* output_matrix = global_output_matrix();
  for (auto _: state) {
    ulvt_transpose_v1(output_matrix, input_matrix);
    benchmark::ClobberMemory();
  }

  for (size_t i = 0; i < matrix_columns; i++) {
    for (size_t j = 0; j < matrix_rows; j++) {
      //assert(plan_output_matrix[i * matrix_rows + j] == plan_input_matrix[j * matrix_columns + i]);
      assert(output_matrix[i * matrix_rows + j] == input_matrix[j * matrix_columns + i]);
      if (output_matrix[i * matrix_rows + j] != input_matrix[j * matrix_columns + i])
        printf("mismatch at (%ld, %ld)\n", i, j);
    }
  }
}
BENCHMARK(ulvt_transpose_v1_bench)
  ->Name("ulvt_transpose_v1")->UseRealTime()->Unit(benchmark::kSecond);

// endregion

//region - MKL

#include "mkl.h"

static void MKL(benchmark::State& state) {
  uint64_t* input_matrix = global_input_matrix();
  uint64_t* output_matrix = global_output_matrix();
  mkl_set_num_threads_local(state.range(0));
  for (auto _: state) {
    mkl_domatcopy(
      'r', 
      't', 
      matrix_rows,
      matrix_columns,
      1.0, 
      (double *)input_matrix,
      matrix_columns,
      (double *)output_matrix,
      matrix_rows
      );
  }

#ifdef DEBUG
  for (size_t i = 0; i < matrix_columns; i++) {
    for (size_t j = 0; j < matrix_rows; j++) {
      assert(output_matrix[i * matrix_rows + j] == input_matrix[j * matrix_columns + i]);
    }
  }
#endif
}
BENCHMARK(MKL)->RangeMultiplier(2)->Range(1, 16)->UseRealTime()->Unit(benchmark::kSecond);

//endregion

// region - eigen

#include <Eigen/Dense>

typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> HermezMatrix;

static void EigenTranspose(benchmark::State& state) {
  HermezMatrix input_matrix = HermezMatrix::Random(matrix_rows, matrix_columns);
  HermezMatrix output_matrix = HermezMatrix::Zero(matrix_columns, matrix_rows);
  for (auto _: state) {
    output_matrix = input_matrix.transpose();
    benchmark::ClobberMemory();
  }

#ifdef DEBUG
  for (size_t i = 0; i < matrix_columns; i++) {
    for (size_t j = 0; j < matrix_rows; j++) {
      assert(output_matrix(i, j) == input_matrix(j, i));
    }
  }
#endif
}
BENCHMARK(EigenTranspose)->Name("Eigen")->UseRealTime()->Unit(benchmark::kSecond);

//endregion

// region - fftw

#include "fftw-shim.h"

shim_fftw_plan plan_transpose(char storage_type,
                         int rows,
                         int cols,
                         double *in,
                         double *out)
{
    const unsigned flags = FFTW_MEASURE | FFTW_DESTROY_INPUT;

    shim_fftw_iodim howmany_dims[2];
    switch (toupper(storage_type)) {
        case 'R':
            howmany_dims[0].n  = rows;
            howmany_dims[0].is = cols;
            howmany_dims[0].os = 1;
            howmany_dims[1].n  = cols;
            howmany_dims[1].is = 1;
            howmany_dims[1].os = rows;
            break;
        case 'C':
            howmany_dims[0].n  = rows;
            howmany_dims[0].is = 1;
            howmany_dims[0].os = cols;
            howmany_dims[1].n  = cols;
            howmany_dims[1].is = rows;
            howmany_dims[1].os = 1;
            break;
        default:
            return NULL;
    }
    const int howmany_rank = sizeof(howmany_dims)/sizeof(howmany_dims[0]);

    return shim_fftw_plan_guru_r2r(/*rank*/0, /*dims*/NULL,
                              howmany_rank, howmany_dims,
                              in, out, /*kind*/NULL, flags);
}

static void fftw(benchmark::State& state) {
  shim_fftw_init_threads();
  shim_fftw_plan_with_nthreads(state.range(0));
  uint64_t* input_matrix = global_input_matrix();
  uint64_t* output_matrix = global_output_matrix();

  char filename[256];
  sprintf(filename, "bench_%ld_%s.wis", state.range(0), __TIME__);

  int wisdom_imported = shim_fftw_import_wisdom_from_filename(filename);

  fftw_plan plan = plan_transpose('R', matrix_rows, matrix_columns, (double*)input_matrix, (double*)output_matrix);

  assert(plan != NULL);

  if (!wisdom_imported) {
    shim_fftw_export_wisdom_to_filename(filename);
  }

  for (auto _: state) {
    shim_fftw_execute_r2r(plan, (double*)input_matrix, (double*)output_matrix);
    benchmark::ClobberMemory();
    //fftw_execute(plan);
  }

#ifdef DEBUG
  for (size_t i = 0; i < matrix_columns; i++) {
    for (size_t j = 0; j < matrix_rows; j++) {
      //assert(plan_output_matrix[i * matrix_rows + j] == plan_input_matrix[j * matrix_columns + i]);
      assert(output_matrix[i * matrix_rows + j] == input_matrix[j * matrix_columns + i]);
    }
  }
#endif
}
BENCHMARK(fftw)->RangeMultiplier(2)->Range(1, 16)->UseRealTime()->Unit(benchmark::kSecond);

// endregion

// region - hptt

#include <iostream>
#include <hptt.h>

static void hptt_bench(benchmark::State& state) {
  uint64_t* input_matrix = global_input_matrix();
  uint64_t* output_matrix = global_output_matrix();
  int dim = 2;
  int perm[dim] = {1, 0};
  int size[dim] = {matrix_rows, matrix_columns};
  auto plan = hptt::create_plan(perm, dim, 1.0, (int64_t*)input_matrix, size, NULL, 0.0, (int64_t*)output_matrix, NULL, hptt::MEASURE, state.range(0), nullptr, true);

  for (auto _: state) {
    plan->execute();
    benchmark::ClobberMemory();
  }

#ifdef DEBUG
  for (size_t i = 0; i < matrix_columns; i++) {
    for (size_t j = 0; j < matrix_rows; j++) {
      //assert(plan_output_matrix[i * matrix_rows + j] == plan_input_matrix[j * matrix_columns + i]);
      if (output_matrix[i * matrix_rows + j] != input_matrix[j * matrix_columns + i]) {
        std::cout << "index " << i << ',' << j << " incorrect" << std::endl;
        std::cout << "input is " << input_matrix[j*matrix_columns+i] << " (" << ((double*)(input_matrix))[j*matrix_columns+i] << ")" << std::endl;
        std::cout << "output is " << output_matrix[i * matrix_rows + j] << " (" << ((double*)(output_matrix))[i * matrix_rows + j] << ")" << std::endl;
      }
      assert(output_matrix[i * matrix_rows + j] == input_matrix[j * matrix_columns + i]);
    }
  }
#endif
}
BENCHMARK(hptt_bench)->Name("hptt")->RangeMultiplier(2)->Range(1, 16)->UseRealTime()->Unit(benchmark::kSecond);

// endregion

BENCHMARK_MAIN();
